# coding=utf-8

'''
家庭作业1： 利用python的opencv 模块读图片，显示图片，显示出图片本身矩阵的值。
'''

## method_1:
import cv2

imageArray = cv2.imread('jj1.jpg')
print(imageArray,type(imageArray),imageArray.shape)

cv2.imshow('give_name',imageArray)
cv2.waitKey(1000*9)
cv2.destroyAllWindows()


## method_2:
import numpy as np
from PIL import Image

img = Image.open('jj1.jpg')
imgData = img.getdata()
width,height = img.size
imgArray = np.array(imgData,dtype=np.uint8).reshape(height,width,3)
print(imgArray,type(imgArray),imgArray.shape)

#print(imageArray == imgArray) rgb,gbr
print(imageArray == imgArray[:,:,::-1]) # True

img.show()
