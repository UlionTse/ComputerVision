# coding=utf-8

'''
homework2: 实现图像灰度化，并显示。然后计算这个灰度图的灰度直方图。
'''

import numpy as np
import cv2
from PIL import Image
from collections import Counter
import matplotlib.pyplot as plt

## method_1:
imgArray = cv2.imread('jj1.png')
grayImgArray = cv2.imread('jj1.png', cv2.IMREAD_GRAYSCALE)
# print(imgArray,imgArray.shape) #(1033,1080,3)
# print(grayImgArray,grayImgArray.shape) #(1033,1080)

# cv2.imwrite('gray_jj1.png', grayImgArray)
cv2.imshow('give_title', grayImgArray)
cv2.waitKey(5*1000)
cv2.destroyAllWindows()


## method_2:
image = Image.open('jj1.png')
grayImage = image.convert('L') #When translating a color image to black and white (mode "L")

# grayImage.save('gray2_jj1.png')
grayImage.show()


def getGrayHist(pixelSequence,color='black'):
    plt.hist(pixelSequence,256,color=color)
    plt.xlabel('grayLevel')
    plt.ylabel('pixelNumber')
    plt.xlim((0,255))
    plt.show()


if __name__ == '__main__':
    pixelSequence1 = grayImgArray.flatten()
    histStat1 = sorted(Counter(pixelSequence1).most_common())
    print(histStat1)

    imageData = grayImage.getdata()
    pixelSequence2 = np.array(imageData)
    histStat2 = sorted(Counter(pixelSequence2).most_common())
    print(histStat2)

    getGrayHist(pixelSequence1)
    getGrayHist(pixelSequence2,color='blue')

