# coding=utf-8
# uliontse
# version 0.3

'''
homework3： 给你2个矩形框,rect1（x1,y1,width1,height1）,rect2(x2,y2,width2,heigth2),请计算这两个矩形框的交集面积和并集面积。
要求，显示结果做好汇出图出来。提示（矩形框的相对位置不做显示，可以交集面积为零，也可以不为零）
'''

import cv2


rect = lambda x,y,width,height: (x,y,width,height)

def getArea(rect1,rect2):
    x_sort = sorted([rect1[0], rect1[0] + rect1[2], rect2[0], rect2[0] + rect2[2]])
    y_sort = sorted([rect1[1], rect1[1] + rect1[3], rect2[1], rect2[1] + rect2[3]])
    x_diff, y_diff = x_sort[2] - x_sort[1], y_sort[2] - y_sort[1]

    result = {
        'intersect': True,
        'data': {
            'intersection_rect': rect(x_sort[1],y_sort[1],x_diff,y_diff),
            'intersection_area': x_diff * y_diff,
            'true_union_area': rect1[2]*rect1[3] + rect2[2]*rect2[3] - x_diff*y_diff,
            'fake_union_area': (x_sort[3] - x_sort[0]) * (y_sort[3] - y_sort[0]),
            'fake_union_rect': rect(x_sort[0],y_sort[0],x_sort[3] - x_sort[0],y_sort[3] - y_sort[0])
            }
        }

    if (rect2[0]<=rect1[0]<rect2[0]+rect2[2] or rect1[0]<=rect2[0]<rect1[0]+rect1[2]) \
            and (rect2[1]<=rect1[1]<rect2[1]+rect2[3] or rect1[1]<=rect2[1]<rect1[1]+rect1[3]):
        return result

    result.update({'intersect': False})
    result['data'].update({'intersection_area':0, 'true_union_area': rect1[2]*rect1[3] + rect2[2]*rect2[3]})
    return result


def overlapRect(imgPath,rect1=None,rect2=None):
    imgArr = cv2.imread(imgPath)
    print(imgArr.shape)
    height,width,_ = imgArr.shape

    if rect1 is not None and rect2 is not None:
        rect1,rect2 = rect1,rect2
    elif (rect1 is None and rect2 is not None) or (rect1 is not None and rect2 is None):
        raise('Parameter Error, [rect1] and [rect2] must exist together.')
    else:
        print('Default drawing based on the image of the [cross rectangle], if no specific rectangle is specified.')
        rect1 = rect(int(width/4),int(5*height/12),int(width/2),int(height/6))
        rect2 = rect(int(5*width/12),int(height/4),int(width/6),int(height/2))

    area = getArea(rect1,rect2)
    print(area)
    rect3 = area['data'].get('intersection_rect')
    intersection_area = area['data'].get('intersection_area')

    cv2.rectangle(imgArr,pt1=(rect1[0],rect1[1]),pt2=(rect1[0]+rect1[2],rect1[1]+rect1[3]),color=(0,0,255),thickness=1)
    cv2.rectangle(imgArr,pt1=(rect2[0],rect2[1]),pt2=(rect2[0]+rect2[2],rect2[1]+rect2[3]),color=(0,255,0),thickness=1)
    cv2.rectangle(imgArr,pt1=(rect3[0],rect3[1]),pt2=(rect3[0]+rect3[2],rect3[1]+rect3[3]),color=(255,0,0),thickness=1)

    cv2.rectangle(imgArr,pt1=(rect3[0],rect3[1]),pt2=(rect3[0]+rect3[2],rect3[1]+16+8),color=(255,0,0),thickness=-1)#fill color
    font = cv2.FONT_HERSHEY_TRIPLEX
    cv2.putText(imgArr,text='intersection_area: {}'.format(intersection_area),org=(rect3[0],rect3[1]+16),
                fontFace=font,fontScale=0.5,color=(255,255,255),thickness=1)

    cv2.imshow('give_name', imgArr)
    cv2.waitKey(1000 * 10)
    cv2.destroyAllWindows()
    return


if __name__ == '__main__':
    rect1 = rect(0,0,200,200)
    rect2 = rect(300,300,200,200)

    # print(getArea(rect1,rect2))
    overlapRect('jj1.png')
    # overlapRect('jj1.png',rect1,rect2)
