# coding=utf-8
# uliontse

'''
homework4： 利用cv2（非plt）画背景有网格的可连线函数，线条有数量，区分颜色。func_plt（x_list, y_list).
'''

import cv2
import numpy as np
import random

def plotLine(canvas=None,linePointArray=None,lineColor=None,showSeconds=0,ifSave=False,**kwargs):

    """
    Parameters
    ----------
    canvas : 3d array-like.

    linePointArray : Nd array-like.

    lineColor : Nd array-like.

    kwargs:

    Returns
    -------
    None

    Examples
    --------
    >>> import cv2
    >>> canvas = cv2.imread('white.png')
    >>> linePointArray = [[(1,2),(3,4)],[(10,20),(30,40),(50,60)]]
    >>> plotLine(canvas,linePointArray)
    """

    canvas = canvas or np.full(shape=(1000,1000,3),fill_value=255,dtype=np.uint8)
    canvasNetColor = kwargs.get('canvasNetColor') or (200,200,200)
    canvasNetThickness = kwargs.get('canvasNetThickness') or 0

    height,width,_ = canvas.shape
    for i in range(0,width,5):
        cv2.line(canvas,pt1=(i,0),pt2=(i,height),color=canvasNetColor,thickness=canvasNetThickness)
    for j in range(0,height,5):
        cv2.line(canvas,pt1=(0,j),pt2=(width,j),color=canvasNetColor,thickness=canvasNetThickness)

    if not linePointArray:
        x = 0
        linePointArray = [[]]
        while x<= width:
            linePointArray[0].append((x,int((x-width/2)**2/200))) #圆锥曲线
            x += 1

    for index,lines in enumerate(linePointArray):
        randomColor = lineColor[index] if lineColor else (random.randint(0, 255),random.randint(0, 255),random.randint(0, 255))
        for i in range(len(lines)-1):
            cv2.line(canvas,pt1=lines[i],pt2=lines[i+1],color=randomColor,thickness=2)

    cv2.imshow('canvas',canvas)
    cv2.waitKey(showSeconds*1000)

    if ifSave: cv2.imwrite('canvas_line.png',canvas)


if __name__ == '__main__':
    plotLine()
